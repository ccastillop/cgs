## Introduction

This web app permits setup and admin the AD6584 Clock Reference card, 
in order to generate stable and precise clock references for radar use.
This app must be installed on the Raspberry Pi

## PRE-setup:

Used inside a Beagle Bone Black. Use minimun debian image in order to flash eMCC on board memory.
http://elinux.org/BeagleBoardDebian#eMMC:_BeagleBone_Black.2FGreen

- download image: https://rcn-ee.com/rootfs/2016-10-06/flasher/BBB-eMMC-flasher-debian-8.6-console-armhf-2016-10-06-2gb.img.xz
- unxz
- burn a microSD with this image (example, with mac: `dd if=<file.img> of=/dev/rdisk4`)
- boot form de BBB: before connect, connect microsd, then push boot button, then connect power, whait the leds flash secuencialy ... 

### Setup Static IP:

- Plug ethernet cable
- `sudo connmanctl services`
- `sudo connmanctl config ethernet_1cba8ce0d4d0_cable --ipv4 manual 10.10.10.215 255.255.255.0 10.10.10.1 --nameservers 8.8.8.8`

## Setup

Install pip: https://pip.pypa.io/en/stable/installing/

    mkdir downloads && cd downloads
    wget https://bootstrap.pypa.io/get-pip.py
    sudo python get-pip.py 

install bottle: `sudo pip install bottle`    

user permitions: `sudo usermod -G i2c debian`

visudo for debian: chekc if debian is sudo

init config

    sudo ln -s /home/debian/apps/jro_clock_reference_webapp/init.sh /etc/init.d/clock_web.sh
    sudo ln -s /home/debian/apps/jro_clock_reference_webapp/display/init.sh /etc/init.d/clock_web_display.sh

log rotate config

    sudo cp /home/debian/apps/jro_clock_reference_webapp/logrotate.conf /etc/logrotate.d/clock_reference.conf


## Obsolete: old setup for Raspberry PI

use pyenv
http://lovebug356.blogspot.pe/2016/01/build-your-own-python-version-on.html

install python 3.4 `pyenv install 3.4.2`



Install i2c modules and tools
`sudo apt-get install python-smbus`

REf: https://learn.adafruit.com/adafruits-raspberry-pi-lesson-4-gpio-setup/configuring-i2c

User pi permisions

`sudo usermod -G i2c pi`

