# first add user permissions to GPIO: http://www.embeddedhobbyist.com/2016/05/beaglebone-black-gpio-permissions/

import Adafruit_BBIO.GPIO as GPIO
import Adafruit_BBIO.ADC as ADC
import time

print("Starting clock sensor")
# inicializa entradas
# led externo
GPIO.setup("P8_7", GPIO.OUT)
# selector clock
GPIO.setup("P9_30", GPIO.OUT)
# entrada sensor clock
ADC.setup()

# bucle de supervision

try:
    while(1):
        voltage = ADC.read("AIN5") * 1.8
        if voltage > 0.5:
            GPIO.output("P8_7", GPIO.HIGH)
            GPIO.output("P9_30", GPIO.HIGH)
        else: 
            GPIO.output("P8_7", GPIO.LOW)
            GPIO.output("P9_30", GPIO.LOW)

        time.sleep(2)

except (KeyboardInterrupt, SystemExit):
    GPIO.cleanup()
    ADC.cleanup()
    print("Closing clock sensor") 
