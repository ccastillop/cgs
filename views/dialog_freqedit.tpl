% # Use Main Template with Title "Frequency Editor"

% from random import random
% rebase dialog title='Frequency Preset %s' % preset


<form id='p{{preset}}_form' data-theme="a">
        % # Editor
        %for num in range(0,len(freqs)):
        
        %# Reformat Frequency into nice human-readable format
        %unit = 1
        %f = freqs[num]
        %if (f % 1E6 == 0):
        %    unit = 1E6
        %    f = int(f / 1E6)
        %end
        %if (f % 1E3 == 0):
        %    unit = 1E3
        %    f = int(f / 1E3)
        %end
        
        <!-- Frequency {{num}} -->
        <p class="freq-editor" >
                <div class="ui-grid-a ui-hide-label ui-responsive" data-role="fieldcontain">
                        <div class="ui-block-a">
                                <h4 style='margin-top:0px'>Output {{num}}: </h4>
                        </div>
                        <div class="ui-block-b">
                                <label for="p{{preset}}f{{num}}_onoff" >State:</label>
                                <select onclick="OnOffFrequency(p{{preset}}f{{num}})" name="p{{preset}}f{{num}}_onoff" id="p{{preset}}f{{num}}_onoff" data-role="slider" data-mini="true" data-theme="c" data-track-theme="a">
                                    <option value="0" {{'selected' if (freqs[num]==0) else ''}}>Off</option>
                                    <option value="1" {{'selected' if (freqs[num]>0) else ''}}>On</option>
                                </select>
                        </div>
                </div>
                <p></p>
                <div class="ui-grid-a ui-hide-label ui-responsive" data-role="fieldcontain">
                        <div class="ui-block-a">
                                <label for="p{{preset}}f{{num}}">Frequency {{num}}</label>
                                <input type="text" name="p{{preset}}f{{num}}" id="p{{preset}}f{{num}}" value="{{f}}" data-mini="true"/>
                        </div>                
                        <div class="ui-block-b">
                                <fieldset data-role="controlgroup" data-type="horizontal" data-mini="true">
                                        <legend>Unit:</legend>
                                        
                                        <input type="radio" name="p{{preset}}f{{num}}_unit" id="p{{preset}}f{{num}}-Hz" value="1" {{'checked' if (unit==1) else ''}} />
                                        <label for="p{{preset}}f{{num}}-Hz">Hz</label>
                
                                        <input type="radio" name="p{{preset}}f{{num}}_unit" id="p{{preset}}f{{num}}-KHz" value="1000" {{'checked' if (unit==1E3) else ''}} />
                                        <label for="p{{preset}}f{{num}}-KHz">KHz</label>
                
                                        <input type="radio" name="p{{preset}}f{{num}}_unit" id="p{{preset}}f{{num}}-MHz" value="1000000" {{'checked' if (unit==1E6) else ''}} />
                                        <label for="p{{preset}}f{{num}}-MHz">MHz</label>
                                
                                </fieldset>
                        </div>
                </div>&nbsp;
                <hr>
        </p>
        % end

        % # Buttons
        <div data-role="fieldcontain" id="buttons"> 
                        <a href="/settings" data-theme="c" data-role="button" data-rel="back" data-mini="true" data-inline="true" data-icon="delete">Discard</a>
                        <a href="/settings?reload={{random()}}" data-role="button" data-direction="reverse" data-mini="true" data-inline="true" data-icon="check" onclick='SaveFrequencies({{preset}})'>Save</a>
                        <a href="/activate/preset/{{preset}}" data-role="button" data-mini="true"  data-inline="true" data-icon="check" onclick='SaveFrequencies({{preset}})'>Save and Activate</a>
        </div>
        
</form>


