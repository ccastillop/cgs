<!doctype html>
<html>
<head>
    <meta charset="UTF-8">
    <title>Dialog</title>
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="stylesheet" href="/static/css/jquery.mobile-1.3.2.css">
    <link rel="stylesheet" href="/static/css/freqref.css">
    <script src="/static/js/jquery-1.9.1.js"></script>
    <script src="/static/js/jquery.mobile-1.3.2.js"></script>
    <script src="/static/js/moment.min.js"></script>
    <script src="/static/js/freqref.js"></script>

</head>
<body>
    <div data-role="dialog" data-theme="b">
 
        <div data-role="header" data-theme="b">

             <!-- Title -->
            <h1>{{ title }}</h1>
           
        </div><!-- /header -->
 

        <div data-role="content" data-theme="b" style='overflow:scroll'>
                
                %if defined('content'):
                {{!content}}
                %else:
                %include
                %end
                                
        </div><!-- /content -->
        
        <div data-role="footer" data-theme="b">
                <a href="/" data-rel="back" data-mini="true" data-role="button" data-inline="true" data-icon="arrow-l">Back</a>
        </div>


    </div><!-- /dialog -->

</body>
</html>