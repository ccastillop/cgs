<!doctype html>
<html>
<head>
    <meta charset="UTF-8">    
    
    <title>JRO Frequency Reference | {{ title or 'No Title' }}</title>
    
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="apple-mobile-web-app-capable" content="yes">
    <link rel="apple-touch-icon" href="/static/icons/apple-touch-icon.png">
    <link rel="apple-touch-startup-image" href="/static/icons/startup.png">
    
    <link rel="icon" href="/static/icons/frequency.png" type="image/png">
   
    <link rel="stylesheet" href="/static/css/jquery.mobile-1.3.2.css">
    <link rel="stylesheet" href="/static/css/freqref.css">
    
    <script src="/static/js/jquery-1.9.1.js"></script>
    <script src="/static/js/jquery.mobile-1.3.2.js"></script>
    <script src="/static/js/moment.min.js"></script>
    <script src="/static/js/freqref.js"></script>

</head>
<body>
    <div data-role="page" id='{{title.replace(" ", "_")}}' data-theme="b">
 
        <div data-role="header" data-theme="b">

             <!-- Title -->
            <h1>JRO FreqRef</h1>
            <!-- Navigation -->
            <div data-role='navbar' data-iconpos="bottom">
            <ul>
                <li><a data-icon="check" href="/status">Status</a></li>
                <li><a data-icon="gear" href="/settings">Settings</a></li>
            </ul>
            </div>

        </div><!-- /header -->
 

        <div data-role="content" data-theme="b">
                <!-- Update Time -->
            	<span class="last-updated">Updated <span class="last-update">(never)</span>.</span>
            	<div style="clear:both"></div>

                <!-- Title -->
                <h2>{{ title or 'No Title' }}</h2>
                        
                <!-- Main Content -->        
                %include
                <!-- END Main Content -->
                
        </div><!-- /content -->


    </div><!-- /page -->
    
</body>
</html>