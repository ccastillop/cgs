#! /bin/bash

# Load required kernel Modules for I2C
# not needed with boeagleboard
# sudo modprobe i2c_bcm2708;
# sudo modprobe i2c_dev

# Start Webserver
# cd /home/pi/apps/jro_clock_reference_webapp/

DIR="/home/debian/apps/jro_clock_reference_webapp"
LOG="$DIR/log/jro_clock_reference_webapp.log"
ERR="$DIR/log/jro_clock_reference_webapp.log"
DAEMON="/home/debian/.pyenv/versions/3.4.2/bin/python $DIR/webserver.py >$LOG 2>$ERR"
DAEMON_NAME="jro_clock_reference_webapp"

DAEMON_USER=debian

PIDFILE="/tmp/$DAEMON_NAME.pid"

. /lib/lsb/init-functions

do_start () {
    log_daemon_msg "Starting system $DAEMON_NAME daemon"
    start-stop-daemon --start --background --pidfile $PIDFILE --make-pidfile \
          --user $DAEMON_USER --chuid $DAEMON_USER \
          --exec "$DAEMON"
    log_end_msg $?
}
do_stop () {
    log_daemon_msg "Stopping system $DAEMON_NAME daemon"
    start-stop-daemon --stop --pidfile $PIDFILE --retry 10
    log_end_msg $?
}

case "$1" in

    start|stop)
        do_${1}
        ;;

    restart|reload|force-reload)
        do_stop
        do_start
        ;;

    status)
        status_of_proc "$DAEMON_NAME" "$DAEMON" && exit 0 || exit $?
        ;;

    *)
        echo "Usage: /etc/init.d/$DAEMON_NAME {start|stop|restart|status}"
        exit 1
        ;;

esac
exit 0



# case $1 in
#    start)
#       $CMD >$LOG 2>$ERR  & echo $! >>$PID_FILE
#       ;;
#     stop)
#       kill `cat $PID_FILE` ;;
#     *)
#       echo "usage: web {start|stop}" ;;
# esac
# exit 0



# python ./webserver.py #&
