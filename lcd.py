import math
from components.i2c.quick2wire_i2c import I2CMaster, writing, reading
from components.logger import log


from subprocess import call
call(["ls"], shell=True)
call(["/usr/sbin/i2cset -y 1 0x3c 0x00 0x38 0x39 0x14 0x78 0x5e 0x6d 0x0c 0x01 0x06 i"], shell=True)
call(["/usr/sbin/i2cset -y 1 0x3c 0x40 0x48 0x4f 0x4c 0x41 i"], shell=True)




_DEBUG_ = 0

class Interface:
    """
    Provides Access to the Display, LEDs and Buttons on the Front Panel
    """

    display = None

    def __init__(self):
        """
        Initializes a Display Class, a Buttons Class and a LEDs class.
        """
        self.display = Display()

class Display:
    """
    UNTESTED CLASS!

    Provides Access to the I2C connected Display on the Front Panel.

    This is not yet tested and verified code because the Display is still in Transit...
    All work copied from Datasheet NHD-C0220BiZ-FSW-FBW-3V3M.pdf.
    """

    address = 0x3c

    def __init__(self):
        """
        Creates new Display Instance

        Tries to autodetect right I2C Bus from Raspberry Pi Board Revision Number.
        If it doesn't work, try hardcoding Bus Device in quick2wire_i2c.py
        """
        log.info("Display instance started. Clearing Display.")
        self.init()
        # Tries to autodetect right I2C Bus from Version Number.
        # If it doesn't work, try hardcoding the Bus Number.


    def init(self):
        """
        Initializes Display.
        Sequence copied from Display Datasheet NHD-C0220BiZ-FSW-FBW-3V3M.pdf.
        """
        log.info("Initializing Display.")
        s = []  # I2C Byte Sequence
        s.append(self.address)  # Slave Address = 0x78
        s.append(0x00)       # ComSend (?)
        s.append(0x38)
        s.append(0x39)
        s.append(0x14)
        s.append(0x78)
        s.append(0x5E)
        s.append(0x6D)
        s.append(0x0C)
        s.append(0x01)
        s.append(0x06)
        self.i2c_send(s)


    def println(self, text):
        """
        Shows a ASCII string on LCD.
        Sequence copied from Display Datasheet NHD-C0220BiZ-FSW-FBW-3V3M.pdf.
        """
        print("Print Text '%s' to Display." % text)
        #d = 0x00
        s = []
        s.append(self.address)
        s.append(0x40)  # Datasend
        s = s + list(bytearray(text, 'utf-8'))
        # for n in range(0, min(20, len(text)) ):
        #     s.append(text[n])
        self.i2c_send(s)

    def i2c_send(self, sequence):
        """
        Sends the given list of Bytes over I2C Bus.
        """

        print('i2c_send(%s)' % (sequence))

        # if '_DEBUG_' in globals():
        #     print('(debug mode, not reading/writing)')
        #     return 0

        sequence = bytes(sequence)
        write_transaction = writing(self.address, sequence)

        with I2CMaster() as i2c:
            i2c.transaction( write_transaction )
