#! /bin/bash
### BEGIN INIT INFO
# Provides:          lcd display
# Required-Start:    $remote_fs $syslog
# Required-Stop:     $remote_fs $syslog
# Default-Start:     2 3 4 5
# Default-Stop:      0 1 6
# Short-Description: Example initscript
# Description:       This file should be used to construct scripts to be
#                    placed in /etc/init.d.
### END INIT INFO

# Load required kernel Modules for I2C
# not needed with boeagleboard
# sudo modprobe i2c_bcm2708;
# sudo modprobe i2c_dev

# Start Webserver
# cd /home/pi/apps/jro_clock_reference_webapp/

#. /etc/rc.d/init.d/functions

USER="debian"
WD="/home/${USER}"
DIR="${WD}/apps/jro_clock_reference_webapp/display"
DAEMON="${DIR}/start.sh"
LOG="${DIR}/log/jro_clock_reference_display.log"
DAEMON_NAME="jro_clock_reference_display"

# The process ID of the script when it runs is stored here:
PIDFILE="/tmp/$DAEMON_NAME.pid"

#PATH=/home/debian/.pyenv/shims:/home/debian/.pyenv/bin:$PATH

 case $1 in
    start)
       ps -p `cat $PIDFILE` > /dev/null 2>&1
       r=$?
       if [ $r -eq 0 ];  then
         echo "$DAEMON_NAME already running..."
         exit 1
       fi
       echo "$DAEMON_NAME starting server"
       /sbin/start-stop-daemon --start --pidfile $PIDFILE \
          --user $USER  \
          -b --make-pidfile \
          --chuid $USER \
          --startas $DAEMON

       #nohup $CMD 1>$LOG 2>$1 & echo $! > $PID_FILE
       #su $USER -c "$CMD 1>$LOG 2>$1 & echo $! > $PID_FILE"
       #cd $DIR
       #$CMD >$LOG  2>$1  & echo $! > $PID_FILE
       ;;
     stop)
       echo "stopping $DAEMON_NAME"
       /sbin/start-stop-daemon --stop --pidfile $PIDFILE --verbose
       #kill `cat $PID_FILE`
       ;;
     *)
       echo "usage:  {start|stop}" ;;
 esac
 exit 0


# python ./webserver.py #&
