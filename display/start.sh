#!/bin/bash

USR="debian"
USR_DIR="/home/$USR"
WORK_DIR="$USR_DIR/apps/jro_clock_reference_webapp/display"
SCRIPT="$WORK_DIR/main.py"
LOG="$WORK_DIR/log/jro_clock_reference_display.log"
DAEMON_NAME="jro_clock_reference_display"
PID_FILE="/tmp/$DAEMON_NAME.pid"
PYENVDIR="$USR_DIR/.pyenv"
CMD="${PYENVDIR}/versions/3.4.2/bin/python $SCRIPT >$LOG 2>&1"
#command without pyenv
#CMD="${PYENVDIR}/versions/3.4.2/bin/python $SCRIPT >$LOG 2>&1"

cd $WORK_DIR
$CMD & echo $! > $PID_FILE
echo "Started $DAEMON_NAME PID: `cat $PID_FILE`"
