from display import Display
from subprocess import call, Popen, PIPE
import time
import requests

#To get the string from list
def list2string(list_data):
    string_data = ''
    cont = 0
    for element in list_data:
        if cont<(len(list_data)-1):
            string_data = string_data + chr(element)
        cont = cont + 1

    return string_data


#IP
ip_data = Popen(["/sbin/ifconfig eth0 | grep 'inet addr:' | cut -d: -f2 | awk '{ print $1}'"],shell=True, stdin=PIPE, stdout=PIPE, stderr=PIPE)
output0, err0 = ip_data.communicate(b"input data that is passed to subprocess' stdin")
ip_data = list(output0)
ip = list2string(ip_data)
ip_data = "IP: "+list2string(ip_data)

#Gateway
gw_data = Popen(["/sbin/route -n | grep 'UG[ \t]' | awk '{print $2}'"],shell=True, stdin=PIPE, stdout=PIPE, stderr=PIPE)
output1, err1 = gw_data.communicate(b"input data that is passed to subprocess' stdin")
gwdata = list(output1)
gateway_data = "GW: "+list2string(gwdata)

#Mask
mask_data = Popen(["/sbin/ifconfig eth0 | awk '/Mask:/{ print $4;} '"],shell=True, stdin=PIPE, stdout=PIPE, stderr=PIPE)
output2, err2 = mask_data.communicate(b"input data that is passed to subprocess' stdin")
mask_data = list(output2)
mask_data = list2string(mask_data)

#Port
port = '8000'
port_data = 'PORT: '+port

#try to active last preset
r1 = "http://"+ip+':'+port+'/activate/preset/0'
print("Intentando "+r1)

while(1):
    lcd = Display()
    lcd.println("Clock Generator", 1, 1)
    lcd.println("& Synchronizer...",2,0)
    time.sleep(1)
    lcd.println("Loading last   ", 1, 1)
    lcd.println("configuration...",2,0)
    time.sleep(1)
    lcd.println("please         ", 1, 1)
    lcd.println("wait ",2,0)
    time.sleep(1)
    try:
        frequencies = requests.get(r1, timeout = 1)
        print("loading last config: " + frequencies.text)
        lcd.println("Last config.", 1, 1)
        lcd.println("loaded... :D",2,0)
        time.sleep(2)
        break
    except Exception as inst:
        print(type(inst))
        print(inst)
        print(inst.args)
        pass


#Frequencies
r = "http://"+ip+':'+port+'/frequencies/'
#status
s = "http://"+ip+':'+port+'/api/ad9548'
#clock_status
cs = "http://"+ip+':'+port+'/status/clock_sensor/'

while(1):
    try:
        frequencies = requests.get(r, timeout = 1)
    except:
        frequencies = None

    if frequencies != None:
        try:
            frequencies = frequencies.json()
            frequencies = frequencies.get("Frecuencias")
            f0 = frequencies.get("f0")
            f1 = frequencies.get("f1")
            f2 = frequencies.get("f2")
            f3 = frequencies.get("f3")
        except Exception as inst:
            print(type(inst))
            print(inst)
            print(inst.args)
            f0 = "Error"
            f1 = "Can not"
            f2 = "detect"
            f3 = "AD9548"
    else:
        f0 = "---"
        f1 = "---"
        f2 = "---"
        f3 = "---"
#print(len(ip))
    f0 = "Clk1: " + str(f0)
    f1 = "Clk2: " + str(f1)
    f2 = "Clk3: " + str(f2)
    f3 = "Clk4: " + str(f3)

    # obtain status
    try:
        status = requests.get(s, timeout = 1)
        status = status.json()
        status = status.get("message")
    except:
        status = {}

    # obtain clock_sensor status
    try:
        print("intentando obtain clock_sensor status")
        clock_sensor_status = requests.get(cs, timeout = 1)
        clock_sensor_status = clock_sensor_status.json()
        clock_sensor_status = clock_sensor_status.get("message")
    except:
        clock_sensor_status = {}

#Print LCD Screen
#while(1):
    lcd = Display()
    lcd.println(ip_data, 1, 1)
    lcd.println(gateway_data,2,0)
    time.sleep(2)

    lcd.println(mask_data,1,1)
    lcd.println(port_data,2,0)
    time.sleep(2)

    lcd.println(f0,1,1)
    lcd.println(f1,2,0)
    time.sleep(2)

    lcd.println(f2,1,1)
    lcd.println(f3,2,0)
    time.sleep(2)

    # print status
    keys = status.keys()
    ind = 1
    for key in keys:
        lcd.println( key+": "+('true' if status[key] else 'false'), ind, ( 0 if ind==2 else 1 ) )
        ind = ind + 1
        if ind > 2:
            ind = 1
            time.sleep(1.5)

    # print clock_sensor status
    keys = clock_sensor_status.keys()
    ind = 1
    for key in keys:
        if type(clock_sensor_status[key])==bool: 
            value = 'true' if clock_sensor_status[key] else 'false'
        else:
            value = clock_sensor_status[key] 
        lcd.println( key+": " + value, ind, ( 0 if ind==2 else 1 ) )
        ind = ind + 1
        if ind > 2:
            ind = 1
            time.sleep(1.5)


#lcd.println(mask_data, 2, 0)
