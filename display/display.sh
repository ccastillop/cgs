#! /bin/bash

# Load required kernel Modules for I2C
# not needed with boeagleboard
# sudo modprobe i2c_bcm2708;
# sudo modprobe i2c_dev

# Start Webserver
# cd /home/pi/apps/jro_clock_reference_webapp/



 case $1 in
    start)
       echo $$ > /tmp/fiorella_display.pid;
       exec 2>&1 /home/debian/.pyenv/versions/3.4.2/bin/python /home/debian/apps/jro_clock_reference_webapp/display/main.py  1>/tmp/fiorella_display.log &
       ;;
     stop)
       kill `cat /tmp/fiorella_display.pid` ;;
     *)
       echo "usage: web {start|stop}" ;;
 esac
 exit 0


