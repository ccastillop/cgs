UPDATE_RATE = 10000
last_update = Date('01-01-2000')
last_update_timer = null
content_update_timer = null

/////////////////////
// Functions for Frequency/Presets Editor
/////////////////////

function SaveFrequencies(preset) {
    // Read out edited Frequencies    
    var i; var freqs = []
    for(i = 0; i<4; i++) {
        freq = parseInt($('#p'+preset+'f'+i).val())
        unit = parseInt($('input:radio:checked[name=p'+preset+'f'+i+'_unit]').val())
        onoff = parseInt($('#p'+preset+'f'+i+'_onoff').val())
        freqs[i] = freq*unit*onoff
    }
    // Send Save Request via AJAX
    $.ajax('/save/preset/'+preset+'/'+freqs[0]+'/'+freqs[1]+'/'+freqs[2]+'/'+freqs[3]+'/')    
}

function OnOffFrequency(basename) {
    
    // Could implement Disabling of Inputs. But actually not necessary.
    return false;
    
}


/////////////////////
// Functions for Automatic Content Updates
/////////////////////

function UpdateContent() {
    console.log(Date()+' Update Content')
    // Update all Status Containers
    containers = $('.status-container');
    $(containers).each(function(idx, div) {
        
        // Load new Content into Status Container...
        //... and update last_update on success.
        url_to_load = '/status/'+$(div).data('component');
        successfn = function(resp, status, xhr) {if (status == "success") last_update = moment(); }
        $(div).load(url_to_load, successfn)
        
    });
    
    // Run this Content Update regularly
    if (content_update_timer) clearTimeout(content_update_timer);
    content_update_timer = setTimeout('UpdateContent()', UPDATE_RATE)
}

function UpdateLastUpdated() {
    //console.log(Date()+' Clock for LastUpdated')
    // Updates the "Last Updated" message
    $('.last-update').text(
        moment().diff(last_update, 'seconds') + ' seconds ago'
    );
    
    // Run this function once per second.
    if (last_update_timer) clearTimeout(last_update_timer);
    last_update_timer = setTimeout('UpdateLastUpdated()', 1000)
}


/////////////////////
// Main Page Initialization
/////////////////////

function NewPageLoaded() {
    // Indicates that a Page has been (re)loaded
    // Disable all running Timers, then start them again.
    // This complicated scheme is necessary because of jQuery Mobile.
    // Just starting a continous Timer on pageload results in *many* timers
    // running in parallel after a few pageloads. Prev. pages are kept in DOM!
    if (content_update_timer) clearTimeout(content_update_timer);
    if (last_update_timer) clearTimeout(last_update_timer);
    content_update_timer = setTimeout('UpdateContent()',1)
    last_update_timer = setTimeout('UpdateLastUpdated()',1)
}

$(document).on('pageinit', NewPageLoaded);
