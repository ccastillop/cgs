#! /usr/bin/env python

from components.ad9548 import AD9548Board
from components.gps import MotorolaGPS
from components.system import SysInfo
from components.presets import PresetManager

from components.logger import log


class Infos:
    """
    Wrapper Class for all System Components Sources.

    Serves as Node between Component Drivers and Webserver,
    provides readily instantiated Access to all Components.')

    See individual Modules for further Details.
    """
    
    ad9548 = AD9548Board()
    gps = MotorolaGPS()
    system = SysInfo()
    presets = PresetManager()
    
    def format_frequencies(self, frequencies):
        """
        Transforms list of 4 Frequencies f into a nice,
        human-readable Format (1 string).
        """
        
        s = "[ "
        
        for f in frequencies:
            if (f == 0):
                f = '(off)'
            elif (f % 1E6 == 0):
                f = '%sMHz' % int(f/1E6)
            elif (f % 1E3 == 0):
                f = '%sKHz' % int(f/1E3)
            else:
                f = '%sHz' % int(f)
            s = s + ('%s | ' % f)
        
        s = s[:-3]+" ]"
        return s
        
        
    def __init__(self):
        log.info('Controller.py: New Infos() instance initialized.')