import json
from .logger import log
    
    
class PresetManager:
    """
    Class that manages 4 presets by reading / writing them to a File.
    """
    
    file = "presets.txt"
    presets = []
    
    
    def __init__(self, file=None):
        """
        Creates new Preset Manager and loads Data from File.
        """
        log.debug('Preset Manager initializing')
        if file!=None:
            self.file=file
        try:
            self.load()
        except:
            log.info("Couldn't load File %s. Creating Empty Presets."
                     % self.file) 
            self.create_empty()
        
        
    def create_empty(self):
        """
        Initializes the Container with four Presets, all set to All-Off
        """
        log.debug('Creating Empty Preset Container...' % (freqs, preset))
        
        self.presets = []
        for num in range(0,4):
            preset = dict(active=(num==0), f=[0,0,0,0])
            self.presets.append(preset)
        
    
    def update_freq(self, preset, freqs):
        """
        Updates Frequency content of given Preset,
        keeping current 'active' status.
        """
        log.debug('Writing new Frequencies %s to Profile %s' % (freqs, preset))
        
        self.presets[preset]['f'] = freqs
    
    
    def activate(self, preset):
        """
        Sets 'active' of given Preset to True,
        sets 'active of all other Presets to False
        """
        log.debug('Activating Profile %s' % preset)
        
        for num in range(0,4):
            if (num == preset):
                self.presets[num]['active'] = True
            else:
                self.presets[num]['active'] = False
                
                
    def deactivate(self, preset):
        """
        Sets 'active' of given Preset to False
        Use this when a Frequency Update went wrong,
        and none of the Presets is currently active.
        """
        log.debug('Deactivation Preset %s. Something gone wrong?!' % (preset))
        
        self.presets[preset]['active'] = False
            

    def load(self):
        """
        Tries to load Preset Data from given File.
        """
        f = open(self.file, 'r')
        content = f.read()
        f.close()
        
        self.presets = json.loads(content)
        
        
    def save(self):
        """
        Tries to save Preset Data to given File
        """
        log.debug('Writing Presets to File %s' % self.file)
        content = json.dumps(self.presets, sort_keys=True,
                             indent=4, separators=(',', ': '))
        
        f = open(self.file, 'w')
        f.write(content)
        f.close()