#!/usr/bin/env python2.7

import math

from .i2c.quick2wire_i2c import I2CMaster, writing, reading

from .logger import log

#_DEBUG_ = 1

class AD9548Board:
    """
    Provides High-Level Access Methods to the AD9548 Evaluation Board.
    """
    # Settings / Configuration
    I2C_Address = 0x4F
    
    # Private Class Variables
    ic = None
        
    def __init__(self):
        """
        Creates new Instance of AD9548Board
        """
        log.info('New AD9548 Board Instance Created.')
        self.ic = AD9548IC(self.I2C_Address)
    
    def disable_outputs(self):
        """
        Disables Output Drivers on CLKOUT0-3
        """
        log.info('Disabling All Outputs.')
        self.ic.disable_outputs()
        
    
    def status(self):
        """
        Returns a tuple (icon, status) consisting of an icon 'alert'/'okay' and a Status Message String indicating the current Mode of Operation:
            - alert: Starting Up (System Clock not yet locked and stable.)
            - alert: System Clock ready, waiting for Lock to PPS Signal (Output Freerunning, Reference B Invalid)
            - alert: System Clock ready, waiting for Lock to PPS Signal (Output Freerunning, Reference B Valid but not Locked)
            - okay: System Ready, Output Valid and Locked to PPS Signal in Phase and Frequency.
        """
        
        log.info("Creating Short Status Notice for AD9548.")
        
        if (not (self.ic.isSysclkStable() and self.ic.isSysclkLocked())):
            return ('alert', 'Starting Up (SYSCLK not yet locked and stable.)')
        if ((not self.ic.isReferenceValid('B') and self.ic.isFreerunning())):
            return ('alert', 'System Clock ready, waiting for Lock to PPS Signal (Output Freerunning, Reference B Invalid)')
        if ((not self.ic.isReferenceValid('B') and (not self.ic.isFreerunning()))):
            return ('alert', 'System Clock ready, waiting for Lock to PPS Signal (Output not Freerunning (?!), Reference B Invalid)')
        if (not (self.ic.isFrequencyLocked() and self.ic.isPhaseLocked())):
            return ('alert', 'System Clock ready, waiting for Lock to PPS Signal (Output Freerunning, Reference B Valid but not Locked)')
        if (self.ic.isFrequencyLocked() and self.ic.isPhaseLocked() and self.ic.isReferenceValid('B')):
            return ('okay', 'System Ready, Output Valid and Locked to PPS Signal in Phase and Frequency.')
        else:
            return ('alert', 'Something is weird with the System Status. Please Check using statusReport().')
    
    
    def info(self):
        """
        Returns a String containing a human-readable report
        about the most important AD9548 Status Registers.
        """
        
        log.info("Creating Full Status Report for AD9548.")
        
        msg  = ("System Health Status:\n--------------------------------------------\n")
        msg += ("System Clock:    Stable?      %s\n                 Locked?      %s\n" % (self.ic.isSysclkStable(), self.ic.isSysclkLocked()))
        msg += ("DPLL Subsystem:  Freerunning? %s\n                 PhaseLock?   %s\n                 FreqLock?    %s\n" % (self.ic.isFreerunning(), self.ic.isPhaseLocked(), self.ic.isFrequencyLocked()))
        msg += ("Reference A:     Valid?       %s\n                 Fault?       %s\n" % (self.ic.isReferenceValid('A'), self.ic.isReferenceFaulty('A')))
        msg += ("Reference B:     Valid?       %s\n                 Fault?       %s\n" % (self.ic.isReferenceValid('B'), self.ic.isReferenceFaulty('B')))
        msg += ("--------------------------------------------")
        return msg
        
        
    def get_output_levels(self):
        """
        Returns current output Levels.
        Reads current settings from AD9548.
        """
        return self.ic.get_output_levels()
    
    
    def get_frequencies(self):
        """
        Returns current output Frequencies.
        They are calculate live from AD9548 Registers.
        """
        return self.ic.get_frequencies()
    
    
    def set_frequencies(self, f1, f2, f3, f4):
        """
        set_frequencies(f1=xxx, f2=xxx, f3=xxx, f4=xxx)
        
        Sets the Outputs CLKOUT0-3 to Frequencies f1-3 in Hz. Setting an Output to Frequency 0 disables the output driver.
        
        Note that not all Frequency combinations are possible,
        because all outputs are post-divided from a common intermediate frequency fDDS
        which is limited between 62.5MHz and 450MHz.
        
        This function automatically calculates the necessary intermediate frequency, if it exists.
        Then all outputs are disabled, fDDS is changed, output dividers are adjusted, and Outputs are reenabled.
        
        If no appropriate intermediate frequency exists this function returns -1,
        if everything goes well it returns 0.
        """
        # Calculate Intermediate Frequency fDDS
        f = [f1, f2, f3, f4]
        
        log.info("Finding Settings for Output Frequencies %s" % f)
        
        fDDS = self.calc_fDDS(f)
        # Check if fDDS is within Limits
        if (not 62.5E6 <= fDDS <= 450E6):
            log.error("AD9548Board Error: Intermediate Frequency fDDS=%d Out of Range for given Frequencies %s." % (fDDS,f))
            return(-1);
        
        log.info("Found valid fDDS = %s" % fDDS)
        
        div = []
        # Calculate Dividers...
        for i in range(0, 4):
            if (f[i] > 0):  div.append(fDDS/f[i])
            else:           div.append(0)             # Set to Zero where output should be disabled
            # ... and check if Q is within Limits
            if (not 0 <= div[i] <= pow(2,30)):
                log.error("AD9548Board Error: Postdivider Q=%s Out of Range for Output %s" % (div[i],i+1))
                return(-1);
                
        log.info("Calculated Post-Dividers Q1-3 = %s" % div)
        log.info("Disabling all Outputs.")
        # Disable Output Drivers
        self.ic.disable_outputs()
        
        log.info("Changing to new fDDS.")
        # Change fDDS using new Profile
        self.ic.set_fDDS(fDDS)
        
        log.info("Changing Post-Dividers and activating Outputs.")
        # Change Dividers, activating desired Outputs
        self.ic.setPostdividers(div)
        
        return 0
                
        
    def calc_fDDS(self, f):
        """
        Calculates a possible intermediate Frequency fDDS for given array of frequencies f. Input and Output in Hz.
        Input Frequencies of Zero are ignored.
        """
        # Catch condition where all f are zero. Outputs will be disabled, use any Frequency.
        if sum(f) == 0:
            log.info('All output frequencies=0, chosing arbitrary fDDS')
            return 62.5E6
        # Create copy of f, ignoring all Frequencies set to 0
        notzero = [x for x in f if x>0]
        # Find Lowest Common Member of all Desired Frequencies
        fDDS = 1
        for f in notzero:
            fDDS = self.lcm(f,fDDS)
        # Catch condition where found fDDS is too small (we can simply scale up!)
        if (fDDS < 62.5E6): fDDS = (int(62.5E6/fDDS)+1)*fDDS
        return fDDS
        

    def gcd(self, a, b):
        """
        Helper Function used in calc_fDDS
        Return greatest common divisor using Euclid's Algorithm.
        """
        while b:      
            a, b = b, a % b
        return a
        
    
    def lcm(self, a, b):
        """
        Helper Function used in calc_fDDS
        Return lowest common multiple.
        """
        return a * b // self.gcd(a, b)
            
    

    
    




class AD9548IC:
    """
    Enables communication with the AD9548 Clock Generator via I2C
    """
    
    ############ CONSTANTS AND NAMES ###############
    
    # Control and Identification
    R_I2C_CONTROL       =    0x0000
    R_REVISION_NUMBER   =    0x0002
    R_DEVICE_ID         =    0x0003
    R_READBACK          =    0x0004
    R_IO_UPDATE         =    0x0005
    # --
    B_SOFT_RESET        =    (1<<5)
    B_IO_UPDATE         =    (1<<0)
                            
    # Synchronization       
    R_SYNC              =    0x0A02
    # --
    B_SYNC_DISTRIBUTION =    (1<<1);
    B_SYNC_SYSCLK       =    (1<<0);
                            
    # System Clock          
    R_SYSCLK_N_DIVIDER  =    0x0101
    
    # Status
    R_STATUS_SYSCLK     =    0x0D01
    R_STATUS_DPLL       =    0x0D0A
    R_STATUS_REFA       =    0x0D0C
    R_STATUS_REFB       =    0x0D0E
    # --
    B_SYSCLK_STABLE     =    (1<<4)
    B_SYSCLK_LOCKED     =    (1<<0)
    B_DPLL_FREQ_LOCK    =    (1<<5)
    B_DPLL_PHASE_LOCK   =    (1<<4)
    B_DPLL_ACTIVE       =    (1<<1)
    B_DPLL_FREERUN      =    (1<<0)
    B_REF_VALID         =    (1<<3)
    B_REF_FAULT         =    (1<<2)
    
    # Output Configuration
    R_DISTRIBUTION_SETTINGS = 0x0400
    
    
    


    ############ High-Level Functions ###############
    
    def isSysclkLocked(self):
        """
        Checks if AD9548 System Clock is Locked
        """
        sysclkStatus = self.readReg(self.R_STATUS_SYSCLK)
        return True if (sysclkStatus & self.B_SYSCLK_LOCKED) else False


    def isSysclkStable(self):
        """
        Checks if AD9548 System Clock is Stable
        """
        sysclkStatus = self.readReg(self.R_STATUS_SYSCLK)
        return True if (sysclkStatus & self.B_SYSCLK_STABLE) else False


    def isFrequencyLocked(self):
        """
        Checks if AD9548 Output Frequency is Locked to Active Reference Frequency
        """
        DPLLStatus = self.readReg(self.R_STATUS_DPLL)
        return True if (DPLLStatus & self.B_DPLL_FREQ_LOCK) else False
        
        
    def isPhaseLocked(self):
        """
        Checks if AD9548 Output Frequency is Locked to Active Reference Phase
        """
        DPLLStatus = self.readReg(self.R_STATUS_DPLL)
        return True if (DPLLStatus & self.B_DPLL_PHASE_LOCK) else False
        
        
    def isFreerunning(self):
        """
        Checks if AD9548 Output Frequency is Freerunning (not Locked to Reference)
        """
        DPLLStatus = self.readReg(self.R_STATUS_DPLL)
        return True if (DPLLStatus & self.B_DPLL_FREERUN) else False
        
        
    def isReferenceValid(self, refName):
        """
        Checks if Reference refName has a valid Signal applied.
        refName can be either 'A' or 'B'.
        """
        if (refName == 'A'):     register = self.R_STATUS_REFA
        elif (refName == 'B'):    register = self.R_STATUS_REFB
        else: raise NameError("refName must be either 'A' or 'B'");
        RefXStatus = self.readReg(register)
        return True if (RefXStatus & self.B_REF_VALID) else False
        
        
    def isReferenceFaulty(self, refName):
        """
        Checks if Reference refName has a faulty Signal applied.
        refName can be either 'A' or 'B'.
        """
        if (refName == 'A'):     register = self.R_STATUS_REFA
        elif (refName == 'B'):    register = self.R_STATUS_REFB
        else: raise NameError("refName must be either 'A' or 'B'");
        RefXStatus = self.readReg(register)
        return True if (RefXStatus & self.B_REF_FAULT) else False
        
        
    def setMultifunctionPin(self, pin, dir, func):
        """
        Set Multifunction Pin Function and Direction.
            pin: Multifunction Pin Number (0-7)
            dir: Pin Direction ('in'/'out')
            func: Pin Function (0-127) according to Table 25/26 p.44/45 in AD9548 Datasheet
        """
        reg = 0x200 + pin
        val = ((dir & 0x01) << 7) | (func & 0x7F)
        self.writeReg(reg, val)
        
    
    def setPostdividers(self, postdiv):
        """
        Set Output Post-Dividers. Expects Array of Length 4 like [PostDiv1, PostDiv2, PostDiv3, PostDiv4].
        A PostDiv of 0 disables the Output.
        """
        assert len(postdiv) == 4
        
        # Addresses of Postdivider Registers 0 - 3
        addresses = [0x0408, 0x040C, 0x0410, 0x0414]
        
        log.info("Setting Post-Dividers %s" % postdiv)
        
        # Write Postdivider Registers
        for i in range(4):
            addr = addresses[i]
            div = int(postdiv[i]-1 if postdiv[i]>0 else 2**30)            # Translation: Zero in Reg = Divide-by-1, Use maximum division for disabled outputs.
            self.writeReg(addr + 0, (div >> 0 ) & 0xFF)
            self.writeReg(addr + 1, (div >> 8 ) & 0xFF)
            self.writeReg(addr + 2, (div >> 16) & 0xFF)
            self.writeReg(addr + 3, (div >> 24) & ((1<<6) - 1))
        
        log.debug("Setting Output-Disable %s" % [postdiv[i]==0 for i in range(4)])
        
        # Distribution Settings Register 0x400 for Power-Down settings
        # Sets a 1 in every OUTx Bit where Post-div[x] == 0
        dist_reg = sum(int(postdiv[i]==0)<<i for i in range(4))
        self.writeReg(self.R_DISTRIBUTION_SETTINGS, dist_reg)

        # Synchronize Clock Distribution, activates Frequency Output
        self.syncClockDistribution()
            
        self.updateIOReg()
        
        
    def disable_outputs(self):
        """
        Disable all Output Drivers. Used before changing Postdivider settings.
        """
        # Distribution Settings Register 0x400 for Power-Down settings.
        # Set Bits 0-3 to 1 to disable all Output.
        log.debug("Setting Output-Disable %s" % [True for i in range(4)])
        
        self.writeReg(self.R_DISTRIBUTION_SETTINGS, 0x0F)

        self.updateIOReg()
        
    
    def set_output_levels(self):
        """
        NOT IMPLEMENTED
        Could set output Drive Strength, Logic Type, Differential / Single-Ended, ...
        Now initial default value is used (probably CMOS normal on all Outputs)
        """
        log.error("Setting Output Levels is not implemented yet." % len(registers))
        
        raise NotImplementedError
        
        
    def get_output_levels(self):
        """
        Returns the output Type and Drive Strength for OUTCLK0-3 as List of Strings.
        """
        
        modes = ['CMOS', 'CMOS/HiZ', 'HiZ/CMOS', 'HiZ', 'LVDS', 'LVPECL']
        result = []
        
        # Read PowerDown Settings Register
        powerDownReg = self.readReg(0x0400)
        
        # Read Output Settings
        for i in range(0,4):
            addr = 0x0404 + i
            data = self.readReg(addr)
            phaseinvert = ' inv' if ((data >> 5) & 0x01) else ''
            polarityinvert = ' inv' if ((data >> 4) & 0x01) else ''
            strength = ' weak' if (((data >> 3) & 0x01) == 0) else ''
            mode = modes[data & ((1 << 3) - 1)]
            setting = mode + strength + polarityinvert + phaseinvert
            # Check if Output is Powered Down
            if ((powerDownReg >> i) & 0x01):
                setting = 'OFF'
            result.append(setting)

        return result
        
    
    def get_frequencies(self):
        """
        Returns the current Output Frequencies by Reading the Device Registers (used for selfCheck and Status Reports).
        """
        # Simulates Frequency Division / Scaling inside DPLL Core:
        # Formula: fDDS = fRef * S / R
        
        log.info("Calculating Output Frequencies by inspecting AD9548 Registers")
        
        fRef = 1 # Hz
        
        # Get R and S Dividers
        R0 = self.readReg(0x061E)
        R1 = self.readReg(0x061F)
        R2 = self.readReg(0x0620)
        R3 = (self.readReg(0x0621) & ((1 << 6) - 1))
        R = int((R3 << 24) | (R2 << 16) | (R1 << 8) | (R0 << 0)) + 1
        log.debug('Divider R = %s' % R)
        
        S0 = self.readReg(0x0622)
        S1 = self.readReg(0x0623)
        S2 = self.readReg(0x0624)
        S3 = (self.readReg(0x0625) & ((1 << 6) - 1))
        S = int((S3 << 24) | (S2 << 16) | (S1 << 8) | (S0 << 0)) + 1
        log.debug('Divider S = %s' % S)
        
        # Calculate fDDS Intermediate Frequency
        fDDS = int(fRef * S / R)         # Ignoring U/V Part
        log.info("Calculated fDDS = %s Hz" % fDDS)
        
        # Now get Postdivider Settings
        log.debug("Getting Post-Dividers")
        Q = []
        addresses = [0x0408, 0x040C, 0x0410, 0x0414]
        for i in range(4):
            addr = addresses[i]
            D0 = self.readReg(addr + 0) + 1   # Correction Value!
            D1 = self.readReg(addr + 1)
            D2 = self.readReg(addr + 2)
            D3 = (self.readReg(addr + 3) & ((1 << 6) - 1))
            div = (D3 << 24) | (D2 << 16) | (D1 << 8) | (D0 << 0)
            Q.append(div)
        
        log.debug('Output Dividers 0-3 = %s' % Q)
        
        freq = [int(fDDS/div) for div in Q]
        log.info("Calculated fOUTCLK = %s" % freq)
        
        return freq        
         
        
    def set_fDDS(self, freq):
        """
        Set intermediate Frequency fDDS to freq.
        Uses an AD9548_profile object to calculate necessary filters
        and register values, then writes these into internal memory.
        """
        
        log.info("Creating new AD9548 Profile for fDDS = %s" % freq)
        # Instantiate Profile for new fDDS. This already calculates all Filters.
        profile = AD9548Profile(freq)     
        # Generate (Addr, Value) dictionary for Registers that need to be changed
        registers = profile.get_registers()
        
        log.debug("Filter calculation returned %s registers to write" % len(registers))
        # Write new Data into Device Memory
        self.writeRegisters(registers)
        
        log.debug("Issuing IO Update to activate new Profile")
        # Update internal Registers
        self.updateIOReg()
        
        
    def syncClockDistribution(self):
        """
        Activates the Frequency Output.
        """
        # AD9548 Synchronizes the Outputs on Falling Edge of Bit 1 of Reg 0x0A02
        self.writeReg(0x0A02, 0x02)
        self.updateIOReg()
        self.writeReg(0x0A02, 0x00)
        self.updateIOReg()       
        
    
    
    ############ Low-Level Functions ############
    
    def __init__(self, address = 0x4F):
        """
        Creates new AD9548_IC Instance with the device at given I2C address.
        
        Tries to autodetect right I2C Bus from Raspberry Pi Board Revision Number.
        If it doesn't work, try hardcoding Bus Device in quick2wire_i2c.py
        """
        self.address = address
        # Tries to autodetect right I2C Bus from Version Number.
        # If it doesn't work, try hardcoding the Bus Number.
    
    
    def writeReg(self, reg, value):
        """
        Writes 8-bit value to 16-bit register address
        """
        # Prepare individual Bytes
        regHi = (0xFF00 & reg) >> 8
        regLo = (0xFF & reg)
        value = (0xFF & value)  # Truncate to 8bit
        
        log.debug('R(0x%04x) = 0x%02x' % (reg, value))
        
        if '_DEBUG_' in globals():
            log.debug('(debug mode, not writing)')
            return
    
        # Create and Execute Write Transaction
        write_register = writing(self.address, [regHi, regLo, value])
        with I2CMaster() as i2c:
            i2c.transaction( write_register )
        
    
    def readReg(self, reg):
        """
        Reads 8-bit value from 16-bit register address
        """
        # Prepare individual Bytes
        regHi = (0xFF00 & reg) >> 8
        regLo = (0xFF & reg)
        
        log.debug('R(0x%04x) = ...' % (reg))

        if '_DEBUG_' in globals():
            log.debug('(debug mode, not reading/writing)')
            return 0
            
        # Create Write Transaction, Create Read Transaction, Execute both
        write_addr = writing(self.address, [regHi, regLo])
        read_byte = reading(self.address, 1) # Read 1 Byte
        with I2CMaster() as i2c:
            result = i2c.transaction( write_addr, read_byte )
        
        # Return Result (first response, first byte)
        
        log.debug('... = %s' % (result[0][0]))
                                
        return result[0][0]
        
        
    def updateIOReg(self):
        """
        Tells AD9548 to transfer pending data from serial I/O buffer register to internal control register.
        Necessary for certain registers (see AD9548 Datasheet p.59)
        """
        self.writeReg(self.R_IO_UPDATE, self.B_IO_UPDATE)
        
                
    def writeRegisters(self, d):
        """
        Transfers all (16-bit address, 8-bit data) pairs from Dict d into AD9548 memory and issues IO Update.
        """
        
        log.debug('Writing %s Registers' % len(d))
        
        for (addr, val) in d:
            self.writeReg(addr, val)
            
        self.updateIOReg()






class AD9548Profile:
    """
    This class saves a AD9548 Reference Profile.
    
    Use AD9548Profile(freq) to create a new instance,
    this calculates all values and filters for the new fDDS.
    Use get_registers() to get a list of
    (16-bit RegisterAddress, 8-bit Value)-tuples with all Values
    that need to be set in the AD9548.
    
    Use set_fDDS(freq) to calculate profile for a different output Frequency.
    
    Directly edit default values in this Class Definition to alter Loop Filter Characteristics and Thresholds ...
    """
        
    sysfreq = int(1E9)      # System Clock Frequency in Hz  1 GHz
    
    priority = 0            # smaller = higher priority 0
    promoted = 0            # keep <= priority          0
    period = int(1E15)      # in femtoseconds           1Hz
    inner_tolerance = 10000 # = 1E6/(tolerance in ppm)  100ppm
    outer_tolerance = 1000  # = 1E6/(tolerance in ppm)  1000ppm
    validation_timer = 10   # in ms                     10ms
    redetect_timer = 3000   # in ms                     3s

    filter_BW = 0.02        # in Hz                     0.02 Hz (=50s)
    filter_PM = 60          # in Degrees                60deg
    filter_Pole_Offset = 1  # in Hz                     1 Hz
    filter_Pole_Attenuation = 3 # in dB                 3dB
    filter_bytes = []       # Calculate with calc_filter()
                            
                            
    R = 0000                # (Desired R)-1
    S = 240000000-1         # (Desired S)-1
    U = 0                   # (Desired U)-1
    V = 0                   # (Desired V)-1
    phase_lock = 10000      # in ps                     10ns
    phase_lock_fill = 15    # Phase Lock Fill Rate      
    phase_lock_drain = 30   # Phase Lock Drain Rate
    freq_lock = 100         # in ps                     100ps
    freq_lock_fill = 32     # Frequency Lock Fill Rate  
    freq_lock_drain = 68    # Frequency Lock Drain Rate
    
    ftw = 67553994410557    # Calculate with freerun_ftw()
    
    reg = dict()            # Calculate with get_registers()              
    
    def __init__(self, freq):
        """
        Instantiate new AD9548 Profile with given Frequency.
        """
        self.set_fDDS(freq)
        
    
    def calc_freerun_FTW(self):
        """
        Calculates ftw from the saved parameters.
        Calculation taken from AD9548 "Profile Designer.xls"
        """
        F3 = 1/self.sysfreq
        F8 = 1.0/(self.period/1E15)
        F14 = self.R+1
        F15 = self.S+1  # HACK: +IF(ISERROR($F$16/$F$17);0;$F$16/$F$17)
                        # but ignored here again!
        
        F18 = 1.0/(F14*F8)
        F19 = F18*F15
        
        self.ftw = int(F3*F19*pow(2,48))
        
        
    def calc_filter(self):
        """
        Calculates filter_bytes from the saved filter_* parameters.
        Calculation taken from AD9548 "Profile Designer.xls"
        """
        F3 = 1.0/self.sysfreq
        F15 = self.S+1
        F21 = self.filter_BW
        F22 = self.filter_PM*0.01
        F23 = self.filter_Pole_Offset
        F24 = self.filter_Pole_Attenuation
        
        B2 = Flf  =  1/(32.0*F3)
        B3 = Kpfd =  1E15
        B4 = Kvco =  1.0/((F3)*(math.pow(2,48)))
        B5 = wp   =  2*math.pi*F21
        B6 = Pm   =  F22
        B7 = w3   =  2*math.pi*F23
        B8 = attn3 = F24
        B9 = S    =  F15   # HACK:
                           # +IF(ISERROR(F16/F17);0;Profile!F16/Profile!F17)
                           # Simply discard Fractional Part. Not used here anyway. 
        B11 = wp2 = wp*wp
        B12 = T1  = ((1/math.cos(Pm))-math.tan(Pm))/wp
        B13 = T3  = math.sqrt(math.pow(10,(attn3/10.0))-1)/w3
        B14 = wc  = (math.tan(Pm)*(T1+T3)/((T1*T3)+(math.pow((T1+T3),2))))*(math.sqrt(1.0+((T1*T3)+(math.pow((T1+T3),2)))/(1.0*(math.pow(math.tan(Pm)*(T1+T3),2))))-1)
        B15 = wc2 = wc*wc
        B16 = T2  = 1/(wc2*(T1+T3))
                
        C1   =  ((T1/T2)*Kpfd*Kvco/(S*wc2))*math.sqrt((1+wc2*T2*T2)/((1+wc2*T1*T1)*(1+wc2*T3*T3)))
        C2   =  C1*((T2/T1)-1)
        R2   =  T2/C2
        C3   =  0.1*C1
        R3   =  T3/C3
    
        alpha = B24 = 1.0/C1
        beta  = B25 = -1.0/(Flf*C1*R2)
        gamma = B26 = -1.0*(1/C1+1/C2)/(Flf*R2)
        delta = B27 = 1.0/(Flf*T3)

        a0_bits = B29  =  16
        a1_bits = B30  =  6
        a2_bits = B31  =  3
        a3_bits = B32  =  4
        b0_bits = B33  =  17
        b1_bits = B34  =  6
        g0_bits = B35  =  17
        g1_bits = B36  =  6
        d0_bits = B37  =  15
        d1_bits = B38  =  5

        alpha1a = B40  =  (-1*int(math.log(B24,10)/math.log(2,10)) if B24<1 else 0)
        alpha1b = B41  =  (min(math.pow(2, B30) - 1, max(0, B40)) if (B24<1) else 0)
        alpha2a = B42  =  (math.ceil(math.log(B24,10)/math.log(2,10)) if (B24>1) else 0)
        alpha2b = B43  =  (min(math.pow(2, B31) - 1 + math.pow(2, B32) - 1, max(0, B42)) if (B24>1) else 0)
        alpha3a = B44  =  (B43 - 2^B31 + 1 if (B43 >= math.pow(2, B31)) else 0)
        alpha2c = B45  =  (math.pow(2, B31)-1 if (B43 >= math.pow(2, B31)) else B43)
        alpha0a = B46  =  round(B24*(math.pow(2, (B29+B41-B45-B44))))
        alpha0b = B47  =  min(math.pow(2, B29)-1, max(1, B46))

        beta1a =  B49  =  -1*int(math.log(abs(B25),10)/math.log(2,10))
        beta1b =  B50  =  min(math.pow(2, B34)-1, max(0, B49))
        beta0a =  B51  =  round(abs(B25)*(math.pow(2, (B33+B50))))
        beta0b =  B52  =  min(math.pow(2, B33)-1, max(1, B51))

        gamma1a = B54  =  -1*int(math.log(abs(B26),10)/math.log(2,10))
        gamma1b = B55  =  min(math.pow(2, B36)-1, max(0, B54))
        gamma0a = B56  =  round(abs(B26)*(math.pow(2, (B35+B55))))
        gamma0b = B57  =  min(math.pow(2, B35)-1, max(1, B56))

        delta1a = B59  =  -1*int(math.log(B27,10)/math.log(2,10))
        delta1b = B60  =  min(math.pow(2, B38)-1, max(0, B59))
        delta0a = B61  =  round(B27*(math.pow(2, (B37+B60))))
        delta0b = B62  =  min(math.pow(2, B37)-1, max(1, B61))

        # Filter Coeffs
        alpha_filter = B64 = B47*(math.pow(2, (-B29-B41+B45+B44)))
        beta_filter  = B65 = -1*B52*(math.pow(2, (-B33-B50)))
        gamma_filter = B66 = -1*B57*(math.pow(2, (-B35-B55)))
        delta_filter = B67 = B62*(math.pow(2, (-B37-B60)))
        
        # Errors
        alpha_error  = B69 = (B64-B24)/B24
        beta_error   = B70 = (B65-B25)/abs(B25)
        gamma_error  = B71 = (B66-B26)/abs(B26)
        delta_error  = B72 = (B67-B27)/B27
                
        # Filter Bytes
        fB11 = (int(B47) & ((1<<8) - 1))
        fB12 = ((int(B47) >> 8) & ((1<<8) - 1))
        fB13 = (int(B41)+((int(B45) >> 2) << 6))
        fB14 = ((int(B45) >> 2) & 0x01) | ((int(B52) & ((1<<7) - 1)) << 1)
        fB15 = ((int(B52) >> 7)  & ((1<<8) - 1))
        fB16 = ((int(B52) >> 15) & ((1<<2) - 1) | (int(B50) << 2))
        fB17 = (int(B57) & ((1<<8) - 1))
        fB18 = ((int(B57) >> 8) & ((1<<8) - 1))
        fB19 = ((int(B57) >> 16) & ((1<<1) - 1)) | (int(B55) << 1)
        fB20 = (int(B62) & ((1<<8) - 1))
        fB21 = ((int(B62) >> 8) & ((1<<7) - 1)) | ((int(B60) & 0x01) << 7)
        fB22 = ((int(B60) >> 1) & ((1<<4) - 1)) | (int(B44) << 4)
        
        self.filter_bytes = [fB11,fB12,fB13,fB14,fB15,fB16,fB17,fB18,fB19,fB20,fB21,fB22]
    
    
    def get_registers(self):
        """
        Calculates reg from the saved/precalculated parameters. Returns list of (addr, val)-tuples.
        Register Addresses are for Profile0. Add Offset if other Profile should be used.
        Calculation taken from AD9548 "Profile Designer.xls"
        """
        period_lo   = ((self.period >> 0 ) & ((1<<24) - 1))
        period_hi   = ((self.period >> 24) & ((1<<26) - 1))
        
        # Clear existing Entries
        self.reg = []
        # Profile Registers
        self.reg.append((0x0600, (self.promoted << 3) | (self.priority & ((1<<2) - 1))))
        self.reg.append((0x0601, (period_lo >> 0 ) & 0xFF))
        self.reg.append((0x0602, (period_lo >> 8 ) & 0xFF))
        self.reg.append((0x0603, (period_lo >> 16) & 0xFF))
        self.reg.append((0x0604, (period_hi >> 0 ) & 0xFF))
        self.reg.append((0x0605, (period_hi >> 8 ) & 0xFF))
        self.reg.append((0x0606, (period_hi >> 16) & 0xFF))
        self.reg.append((0x0607, (period_hi >> 24) & 0xFF))
        self.reg.append((0x0608, (self.inner_tolerance >> 0 ) & 0xFF))
        self.reg.append((0x0609, (self.inner_tolerance >> 8 ) & 0xFF))
        self.reg.append((0x060A, (self.inner_tolerance >> 16) & 0xFF))
        self.reg.append((0x060B, (self.outer_tolerance >> 0 ) & 0xFF))
        self.reg.append((0x060C, (self.outer_tolerance >> 8 ) & 0xFF))
        self.reg.append((0x060D, (self.outer_tolerance >> 16) & 0xFF))
        self.reg.append((0x060E, (self.validation_timer >> 0 ) & 0xFF))
        self.reg.append((0x060F, (self.validation_timer >> 8 ) & 0xFF))
        self.reg.append((0x0610, (self.redetect_timer >> 0 ) & 0xFF))
        self.reg.append((0x0611, (self.redetect_timer >> 8 ) & 0xFF))
        self.reg.append((0x0612, int(self.filter_bytes[0]) & 0xFF))
        self.reg.append((0x0613, int(self.filter_bytes[1]) & 0xFF))
        self.reg.append((0x0614, int(self.filter_bytes[2]) & 0xFF))
        self.reg.append((0x0615, int(self.filter_bytes[3]) & 0xFF))
        self.reg.append((0x0616, int(self.filter_bytes[4]) & 0xFF))
        self.reg.append((0x0617, int(self.filter_bytes[5]) & 0xFF))
        self.reg.append((0x0618, int(self.filter_bytes[6]) & 0xFF))
        self.reg.append((0x0619, int(self.filter_bytes[7]) & 0xFF))
        self.reg.append((0x061A, int(self.filter_bytes[8]) & 0xFF))
        self.reg.append((0x061B, int(self.filter_bytes[9]) & 0xFF))
        self.reg.append((0x061C, int(self.filter_bytes[10]) & 0xFF))
        self.reg.append((0x061D, int(self.filter_bytes[11]) & 0xFF))
        self.reg.append((0x061E, (self.R >> 0 ) & 0xFF))
        self.reg.append((0x061F, (self.R >> 8 ) & 0xFF))
        self.reg.append((0x0620, (self.R >> 16 ) & 0xFF))
        self.reg.append((0x0621, (self.R >> 24 ) & 0xFF))
        self.reg.append((0x0622, (self.S >> 0 ) & 0xFF))
        self.reg.append((0x0623, (self.S >> 8 ) & 0xFF))
        self.reg.append((0x0624, (self.S >> 16) & 0xFF))
        self.reg.append((0x0625, (self.S >> 24) & 0xFF))
        self.reg.append((0x0626, (self.V >> 0 ) & 0xFF))
        self.reg.append((0x0627, (self.V >> 8 ) & ((1<<4) - 1) | (((self.U >> 0) & ((1<<4) - 1)) << 4)))
        self.reg.append((0x0628, (self.U >> 4 ) & 0xFF))
        self.reg.append((0x0629, (self.phase_lock >> 0 ) & 0xFF))
        self.reg.append((0x062A, (self.phase_lock >> 8 ) & 0xFF))
        self.reg.append((0x062B, (self.phase_lock_fill >> 0 ) & 0xFF))
        self.reg.append((0x062C, (self.phase_lock_drain >> 0 ) & 0xFF))
        self.reg.append((0x062D, (self.freq_lock >> 0 ) & 0xFF))
        self.reg.append((0x062E, (self.freq_lock >> 8 ) & 0xFF))
        self.reg.append((0x062F, (self.freq_lock >> 16) & 0xFF))
        self.reg.append((0x0630, (self.freq_lock_fill >> 0 ) & 0xFF))
        self.reg.append((0x0631, (self.freq_lock_drain >> 0 ) & 0xFF))
        self.reg.append((0x0005, 0x01))  # Update IO Reg
        

        # System Registers
        # System Clock Period
        sysperiod = int(1E15/self.sysfreq) & ((1<<21) - 1)
        self.reg.append((0x0103, (sysperiod >> 0) & 0xFF))
        self.reg.append((0x0104, (sysperiod >> 8 ) & 0xFF))
        self.reg.append((0x0105, (sysperiod >> 16 ) & 0xFF))
        self.reg.append((0x0005, 0x01))  # Update IO Reg
        
        
        # Free-running FTW
        ftw_lo = ((self.ftw >> 0 ) & ((1<<24) - 1))
        ftw_hi = ((self.ftw >> 24) & ((1<<24) - 1))
        self.reg.append((0x0300, (ftw_lo >> 0) & 0xFF))
        self.reg.append((0x0301, (ftw_lo >> 8 ) & 0xFF))
        self.reg.append((0x0302, (ftw_lo >> 16 ) & 0xFF))
        self.reg.append((0x0303, (ftw_hi >> 0) & 0xFF))
        self.reg.append((0x0304, (ftw_hi >> 8 ) & 0xFF))
        self.reg.append((0x0305, (ftw_hi >> 16 ) & 0xFF))
        self.reg.append((0x0306, 0x01))  # Update Tuning Word (even if locked)
        self.reg.append((0x0005, 0x01))  # Update IO Reg
        

        return self.reg
        
        
    def set_fDDS(self, freq):
        """
        Updates the Profile for a new fDDS Output frequency.
        freq must be between 62.5E6 and 450E6.
        This function triggers all necessary internal Filter Calculations.
        """
        assert 62.5E6 <= freq <= 450E6
        
        self.S = int(freq - 1)
        
        self.calc_filter()
        self.calc_freerun_FTW()






# Test Environment: Commandline-Like Interface

if __name__ == '__main__':
    
    import cmd
    
    ad9548 = AD9548Board()
    
    class Interpreter(cmd.Cmd):
        """
        Allows Commandline Interaction with the Functions for AD9548.
        """
        
        def do_EOF(self, line):
            """End Interpreter on ctrl-D"""
            return True
            
        def do_read(self, reg):
            """read reg
            Reads the specified Register reg
            """
            try:
                reg = int(reg, 0) & 0xFFFF  # Also parses Hex Values with '0x...'
                val = ad9548.ic.readReg(reg)
                print("read R(0x%x) = 0x%x = %d" % (reg, val, val))
            except:
                print('Specify reg as 16-bit Hex (0x....) or Decimal.')
            
        def do_write(self, line):
            """write reg val
            Writes val to the specified Registers reg
            """
            line = self.parseline(line)
            print(line)
            try:
                reg = line[0]
                val = line[1]
                reg = int(reg, 0) & 0xFFFF   # Also parses Hex Values with '0x...'
                val = int(val, 0) & 0xFF
                ad9548.ic.writeReg(reg, val)
                print("wrote R(0x%x) = 0x%x = %d" % (reg, val, val))
            except:
                print('Specify reg as 16-bit Hex (0x....) or Decimal.')
                print('Specify val as 8-bit Hex (0x..) or Decimal.')
        
        def do_status(self, line):
            """status
            Prints formatted short status Message.
            """
            print(ad9548.status())
        
        
        def do_info(self, line):
            """info
            Prints formatted long info Message.
            """
            print(ad9548.info())
            
        
        def do_update(self, line):
            """update
            Issues IO Register Update Command
            """
            ad9548.ic.updateIOReg()
    
    # Start the Commandline Interpreter, loops until ctrl-D
    
    Interpreter().cmdloop("AD9548 Command Interpreter.\n----------------------")