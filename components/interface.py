import math

from i2c.quick2wire_i2c import I2CMaster, writing, reading
from

from logger import log

#_DEBUG_ = 1


# Definitions for MCP23008
OUTPUT = 0
INPUT = 1

LED_HEARTBEAT = 0
LED_FREQLOCK = 1
LED_PPS = 2
LED_PWR = 3

BTN_RESET = 4
BTN_PRESET1 = 5
BTN_PRESET2 = 6
BTN_PRESET3 = 7


class Interface:
    """
    Provides Access to the Display, LEDs and Buttons on the Front Panel
    """

    # Constants
    MCP23008_ADDR = 0x20

    # Public Class Variables
    ioexpander = None
    display = None
    buttons = None
    leds = None

    def __init__(self):
        """
        Initializes a Display Class, a Buttons Class and a LEDs class.
        """
        self.display = Display()
        self.ioexpander = MCP23008(self.MCP23008_ADDR)
        self.buttons = Buttons(self.ioexpander)
        self.leds = LEDs(self.ioexpander)


class LEDs(object):
    """
    Manages the LEDs connected to A0-A3 on the MCP23008.
    """

    chip = None
    states = [0, 0, 0, 0]

    def __init__(self, ioexpander):
        """
        Creates new Instance of LEDs class. Argument tells which MCP23008
        object we should use for communication.
        """
        self.chip = ioexpander
        self.set_pin_directions()
        self.all_off()


    def set_pin_directions(self):
        """
        Sets the Pins connected to LEDs as Outputs.
        """
        self.chip.config(0, OUTPUT)
        self.chip.config(1, OUTPUT)
        self.chip.config(2, OUTPUT)
        self.chip.config(3, OUTPUT)


    def all_off(self):
        """
        Turns off all the LEDs.
        """
        self.off(0)
        self.off(1)
        self.off(2)
        self.off(3)
        self.states = [0, 0, 0, 0]


    def on(self, num):
        """
        Turns LED 'num' on.
        """
        self.chip.output(num, 1)
        self.states[num] = 1


    def off(self, num):
        """
        Turns LED 'num' off.
        """
        self.chip.output(num, 0)
        self.states[num] = 0


    def toggle(self, num):
        """
        Toggles the specified LED.
        """
        new_state = int(not self.states[num])
        self.chip.output(num, new_state)
        self.states[num] = new_state



class Buttons(object):
    """
    Manages the Buttons on the
    """

    mcp = None

    states = [0, 0, 0, 0]

    def __init__(self, ioexpander):
        """
        Creates new Instance of LEDs class. Argument tells which MCP23008
        object we should use for communication.
        """
        self.mcp = ioexpander
        self.set_pin_directions()
        self.config_mcp23008_interrupt()


    def set_pin_directions(self):
        """
        Sets the Pins connected to LEDs as Outputs.
        """
        self.mcp.config(4, INPUT)
        self.mcp.config(5, INPUT)
        self.mcp.config(6, INPUT)
        self.mcp.config(7, INPUT)


    def config_mcp23008_interrupt(self):
        """
        Configures the MCP23008 Interrupt for On-Change,
        Outputting a Push-Pull Signal.
        """


    def wait_for_press(self):
        """
        Main Blocking Loop for the Button Interrupts.
        Execute this continuously in a while(true) Loop.
        """





class MCP23008:
    """
    UNTESTED CLASS!

    Communicates with a MCP23008 IO Expander.
    Based upon code by Adafruit found at
    (https://github.com/adafruit/Adafruit-Raspberry-Pi-Python-Code)
    """

    MCP23008_IODIRA = 0x00
    MCP23008_GPIOA  = 0x09
    MCP23008_GPPUA  = 0x06
    MCP23008_OLATA  = 0x0A


    def __init__(self, address=0x20):
        self.i2c = Adafruit_I2C(address=address)
        self.address = address
        self.num_gpios = 8

        # set defaults
        self.i2c.write8(self.MCP23017_IODIRA, 0xFF)  # all inputs on port A
        self.direction = self.i2c.readU8(self.MCP23017_IODIRA)
        self.i2c.write8(self.MCP23008_GPPUA, 0x00)


    def _changebit(self, bitmap, bit, value):
        assert value == 1 or value == 0, "Value is %s must be 1 or 0" % value
        if value == 0:
            return bitmap & ~(1 << bit)
        elif value == 1:
            return bitmap | (1 << bit)

    def _readandchangepin(self, port, pin, value, currvalue = None):
        assert pin >= 0 and pin < self.num_gpios,\
            "Pin number %s is invalid, only 0-%s are valid"\
            % (pin, self.num_gpios)
        if not currvalue:
             currvalue = self.i2c.readU8(port)
        newvalue = self._changebit(currvalue, pin, value)
        self.i2c.write8(port, newvalue)
        return newvalue


    def pullup(self, pin, value):
        return self._readandchangepin(self.MCP23008_GPPUA, pin, value)

    # Set pin to either input or output mode
    def config(self, pin, mode):
        self.direction = self._readandchangepin(self.MCP23008_IODIRA, pin, mode)
        return self.direction

    def output(self, pin, value):
        self.outputvalue = self._readandchangepin(self.MCP23008_GPIOA,
                            pin, value, self.i2c.readU8(self.MCP23008_OLATA))
        return self.outputvalue

    def input(self, pin):
        assert pin >= 0 and pin < self.num_gpios,\
                "Pin number %s is invalid, only 0-%s are valid"
                % (pin, self.num_gpios)
        assert self.direction & (1 << pin) != 0, "Pin %s not set to input" % pin

        value = self.i2c.readU8(self.MCP23008_GPIOA)
        return value & (1 << pin)


    def readU8(self):
        result = self.i2c.readU8(self.MCP23008_OLATA)
        return(result)

    def readS8(self):
        result = self.i2c.readU8(self.MCP23008_OLATA)
        if (result > 127): result -= 256
        return result

    def write8(self, value):
        self.i2c.write8(self.MCP23008_OLATA, value)



class Display:
    """
    UNTESTED CLASS!

    Provides Access to the I2C connected Display on the Front Panel.

    This is not yet tested and verified code because the Display is still in Transit...
    All work copied from Datasheet NHD-C0220BiZ-FSW-FBW-3V3M.pdf.
    """

    addr = 0x78

    def __init__(self, address = 0x4F):
        """
        Creates new Display Instance

        Tries to autodetect right I2C Bus from Raspberry Pi Board Revision Number.
        If it doesn't work, try hardcoding Bus Device in quick2wire_i2c.py
        """
        log.info("Display instance started. Clearing Display.")
        self.init()
        # Tries to autodetect right I2C Bus from Version Number.
        # If it doesn't work, try hardcoding the Bus Number.


    def init(self):
        """
        Initializes Display.
        Sequence copied from Display Datasheet NHD-C0220BiZ-FSW-FBW-3V3M.pdf.
        """
        log.info("Initializing Display.")
        s = []  # I2C Byte Sequence
        s.append(self.addr)  # Slave Address = 0x78
        s.append(0x00)       # ComSend (?)
        s.append(0x38)
        s.append(0x39)
        s.append(0x14)
        s.append(0x78)
        s.append(0x5E)
        s.append(0x6D)
        s.append(0x0C)
        s.append(0x01)
        s.append(0x06)
        self.i2c_send(s)


    def println(self, text):
        """
        Shows a ASCII string on LCD.
        Sequence copied from Display Datasheet NHD-C0220BiZ-FSW-FBW-3V3M.pdf.
        """
        log.info("Print Text '%s' to Display." % text)
        d = 0x00
        s = []
        s.append(self.addr)
        s.append(0x40)  # Datasend
        for n in range(0, min(20, len(text)) ):
            s.append(text[n])
        self.i2c_send(s)


    def i2c_send(self, sequence):
        """
        Sends the given list of Bytes over I2C Bus.
        """

        log.debug('i2c_send(%s)' % (sequence))

        if '_DEBUG_' in globals():
            log.debug('(debug mode, not reading/writing)')
            return 0

        sequence = bytes(sequence)
        write_transaction = writing(self.address, sequence)

        with I2CMaster() as i2c:
            i2c.transaction( write_transaction )
