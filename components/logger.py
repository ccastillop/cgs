import logging

def create_logger(name):
    """
    Returns a new Logger, configured according to whole System needs.
    """    
    mylogger = logging.getLogger(name)
    mylogger.setLevel(logging.DEBUG)

    formatter = logging.Formatter('%(asctime)s - Module %(module)s - Function %(funcName)s - %(levelname)s - %(message)s')

    # Log Warnings + Errors to File
    filehandler = logging.FileHandler('log/error.log')
    filehandler.setLevel(logging.WARNING)
    filehandler.setFormatter(formatter)
    mylogger.addHandler(filehandler)
    
    # Log Debug Messages to File
    filehandler = logging.FileHandler('log/debug.log')
    filehandler.setLevel(logging.DEBUG)
    filehandler.setFormatter(formatter)
    mylogger.addHandler(filehandler)
    
    # Log INFO Messages to File
    filehandler = logging.FileHandler('log/info.log')
    filehandler.setLevel(logging.INFO)
    filehandler.setFormatter(formatter)
    mylogger.addHandler(filehandler)
    
    # Log to Console
    stderr = logging.StreamHandler()
    stderr.setLevel(logging.WARNING)
    stderr.setFormatter(formatter)
    mylogger.addHandler(stderr)
    
    return mylogger


# Log a first Message for this Module
log = create_logger('MAIN_LOGGER')
